package com.example.swornim.frecall;

import android.app.Service;
import android.app.assist.AssistStructure;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.swornim.frecall.DataStructure.DownloadTask;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by swornim on 11/30/17.
 */

public class ReceiveMessageService extends Service {

    private DatabaseReference friendDatabaseNode;
    private DatabaseReference newDataNode;
    private List<MinorDetails> messagesList=new ArrayList<>();
    private List<MinorDetails> newNodeList=new ArrayList<>();
    private List<DownloadTask> downloadTasks=new ArrayList<>();//maintains the download information#solves errors as well
    private static  int threadID=0;
    private File downloadedPath;
    private static int TOTAL_PHOTOS_TO_DOWNLOAD=0;
    private static int TOTAL_PHOTOS_DOWNLOADED=0;
    private Handler downloadHandler;
    private Runnable runnable;
    private boolean handlerRunning;
    List<MinorDetails> messageObjects=new ArrayList<>();



    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        /*only one or zero instance of service will run*/
        /* But the onstartcommand gets called everytime startservice is called*/

        Log.i("mytag","receive service called");

        if(intent.getStringExtra("intentName").equals("receiveMessageService")) {

            downloadHandler=new Handler();
            runnable=new Runnable() {
                @Override
                public void run() {

                    if(TOTAL_PHOTOS_DOWNLOADED==TOTAL_PHOTOS_TO_DOWNLOAD){
                        Log.i("mytag"," downloading finished successfully");

                        for(int i=0;i<downloadTasks.size();i++){
                            Log.i("mytag"," Message Object"+i +":"+new Gson().toJson(downloadTasks.get(i).getMessageObject()));
                            for(int j=0;j<downloadTasks.get(i).getPhotoLocation().size();j++){
                                Log.i("mytag"," Location for "+i +"is "+":"+downloadTasks.get(i).getPhotoLocation().get(j));
                            }

                        }
                        downloadHandler.removeCallbacks(runnable);

                        // TODO: 1/24/18 Handle error like no wifi or service closed
                        /*Chance firebaseUrl to local path and then Update database */
                        MinorDetails messageObject=null;

                        for(int i=0;i<downloadTasks.size();i++){
                             downloadTasks.get(i).getMessageObject().setSeen("true");//update database as seen
                             messageObject=downloadTasks.get(i).getMessageObject();

                            for(int j=0;j<downloadTasks.get(i).getPhotoLocation().size();j++){
                                messageObject.getUploadImages().get(j).setpUrl(downloadTasks.get(i).getPhotoLocation().get(j));
                                Log.i("mytag","local paths are "+messageObject.getUploadImages().get(j).getpUrl());
                            }
                            messageObjects.add(messageObject);

                        }

                        Log.i("mytag","Final MessageObject "+new Gson().toJson(messageObject));

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    for (MinorDetails eachMessage : messageObjects) {
                                        new MSQLiteDatabase(getApplicationContext()).insertIntoCache(eachMessage);
                                    }
                                    Log.i("mytag","Service has ended ");
                                    stopService(intent);
                                }
                            }).start();


                    }else{
                        Log.i("mytag","Still downloading");
                        //update the notification bar its downloading
                        downloadHandler.postDelayed(runnable,1500);

                    }

                }
            };
            handlerRunning=false;

            FirebaseDatabase.getInstance().getReference()
                    .child("users/9841339287/friends/9813847444/messages")
                    .orderByChild("seen")
                    .equalTo("false")
                    .addChildEventListener(new ChildEventListener() {

                        @Override
                        public void onChildAdded(final DataSnapshot dataSnapshot, String s) {
                            Log.i("mytag", "receive datasnapshot " + dataSnapshot);
                            Log.i("mytag", "call back time diff  " + System.currentTimeMillis());
                            //for each call run the download task and update the database json firstly
                            TOTAL_PHOTOS_TO_DOWNLOAD+=dataSnapshot.getValue(MinorDetails.class).getUploadImages().size();
                            runDownloadTasks(dataSnapshot);
                            if(handlerRunning){
                                //do nothing

                            }else{
                                downloadHandler.postDelayed(runnable,10000);
                                handlerRunning=true;
                            }
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

        }
        return START_NOT_STICKY;
    }


    private void runDownloadTasks(final DataSnapshot newDataSnapShot){
        Log.i("mytag", "Two times called  " );

                final int downloadedCount=0;
                DownloadTask eachTask=new DownloadTask();
                List<String> localPaths=new ArrayList<>();
                eachTask.setMessageObject(newDataSnapShot.getValue(MinorDetails.class));

                for(int i=0;i<eachTask.getMessageObject().getUploadImages().size();i++) {

                    downloadedPath = new File(Environment.getExternalStorageDirectory(), "/freecall/" + eachTask.getMessageObject().getUploadImages().get(i).getpName());
                    if (!downloadedPath.exists()) {
                        try {
                            downloadedPath.createNewFile();
                            localPaths.add(downloadedPath.getPath());
                            FirebaseStorage.getInstance()
                                    .getReferenceFromUrl(newDataSnapShot.getValue(MinorDetails.class).getUploadImages().get(i).getpUrl())
                                    .getFile(downloadedPath).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                    ++TOTAL_PHOTOS_DOWNLOADED;
                                    Log.i("mytag", "total bytes downloaded " + taskSnapshot.getBytesTransferred());


                                }
                            });
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }else{
                        Log.i("mytag", "Path exist " );
                        //delete the file
                        downloadedPath.delete();

                        localPaths.add(downloadedPath.getPath());
                        FirebaseStorage.getInstance()
                                .getReferenceFromUrl(newDataSnapShot.getValue(MinorDetails.class).getUploadImages().get(i).getpUrl())
                                .getFile(downloadedPath).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                ++TOTAL_PHOTOS_DOWNLOADED;
                                Log.i("mytag", "total bytes downloaded " + taskSnapshot.getBytesTransferred());


                            }
                        });

                    }
                }
                eachTask.setPhotoLocation(localPaths);
                downloadTasks.add(eachTask);

            }

    }


package com.example.swornim.frecall;

import android.os.Parcelable;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by Swornim on 5/8/2017.
 */

public class MinorDetails implements Serializable {




    //callogs varaibles
    private String userContactID;
    private String userContactName;
    private String userContactNumber;
    private String userMessage;
    private String hhmmss;
    private List<MinorDetails> postdata;
    private List<String> indexes;
    private String seen;


    //phonecalls adapter variables
    private String contactName;
    private String contactNumber;
<<<<<<< HEAD
    private String phoneImageUrl;
=======
    private String localUrl;//location of the phone
>>>>>>> first_branch


    //OFFLINE JSON MESSAGES
    private String offlineJsonMessages;//todo you dont need this incoming or outgoing variables just use one
    private String incomingMessageJ;//for getting from firebase to json and to db
    private String outgoingMessageJ;//for showing from db to phone when misscalled


    //outgoing and incoming message format
    private String message;//for message1 ,message2,message3
    private String index;
    private List<MinorDetails> uploadImages;
    private String imageT;
    private String pUrl;
    private String pName;
    private String vUrl;
    private Map<String,MinorDetails> messages;
    private List<MinorDetails> messageList;
    //this is single block of message which holds entire message1,2,3 in it


<<<<<<< HEAD
    public String getPhoneImageUrl() {
        return phoneImageUrl;
    }

    public void setPhoneImageUrl(String phoneImageUrl) {
        this.phoneImageUrl = phoneImageUrl;
=======


    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getLocalUrl() {
        return localUrl;
    }

    public void setLocalUrl(String localUrl) {
        this.localUrl = localUrl;
    }

    public String getSeen() {
        return seen;
    }

    public void setSeen(String seen) {
        this.seen = seen;
    }

    public List<String> getIndexes() {
        return indexes;
    }

    public void setIndexes(List<String> indexes) {
        this.indexes = indexes;
>>>>>>> first_branch
    }

    public List<MinorDetails> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<MinorDetails> messageList) {
        this.messageList = messageList;
    }

    public String getIncomingMessageJ() {
        return incomingMessageJ;
    }

    public void setIncomingMessageJ(String incomingMessageJ) {
        this.incomingMessageJ = incomingMessageJ;
    }

    public String getOutgoingMessageJ() {
        return outgoingMessageJ;
    }

    public void setOutgoingMessageJ(String outgoingMessageJ) {
        this.outgoingMessageJ = outgoingMessageJ;
    }

    public void setMessages(Map<String, MinorDetails> messages) {
        this.messages = messages;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public List<MinorDetails> getUploadImages() {
        return uploadImages;
    }

    public void setUploadImages(List<MinorDetails> uploadImages) {
        this.uploadImages = uploadImages;
    }

    public String getImageT() {
        return imageT;
    }

    public void setImageT(String imageT) {
        this.imageT = imageT;
    }

    public String getpUrl() {
        return pUrl;
    }

    public void setpUrl(String pUrl) {
        this.pUrl = pUrl;
    }

    public String getvUrl() {
        return vUrl;
    }

    public void setvUrl(String vUrl) {
        this.vUrl = vUrl;
    }


    public Map<String, MinorDetails> getMessages() {
        return messages;
    }

    public static int getUniqueAlarmPendingId() {
        return uniqueAlarmPendingId;
    }

    public static void setUniqueAlarmPendingId(int uniqueAlarmPendingId) {
        MinorDetails.uniqueAlarmPendingId = uniqueAlarmPendingId;
    }


    //misscall interfaces variables
    public String getOfflineJsonMessages() {
        return offlineJsonMessages;
    }

    public void setOfflineJsonMessages(String offlineJsonMessages) {
        this.offlineJsonMessages = offlineJsonMessages;
    }

    public static int uniqueAlarmPendingId=111;


    public List<MinorDetails> getPostdata() {
        return postdata;
    }

    public void setPostdata(List<MinorDetails> postdata) {
        this.postdata = postdata;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getHhmmss() {
        return hhmmss;
    }

    public void setHhmmss(String hhmmss) {
        this.hhmmss = hhmmss;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public String getUserContactName() {
        return userContactName;
    }

    public void setUserContactName(String userContactName) {
        this.userContactName = userContactName;
    }

    public String getUserContactID() {
        return userContactID;
    }

    public void setUserContactID(String userContactID) {
        this.userContactID = userContactID;
    }

    public String getUserContactNumber() {
        return userContactNumber;
    }

    public void setUserContactNumber(String userContactNumber) {
        this.userContactNumber = userContactNumber;
    }
}

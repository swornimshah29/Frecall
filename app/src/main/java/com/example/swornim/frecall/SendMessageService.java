package com.example.swornim.frecall;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by swornim on 11/30/17.
 */

public class SendMessageService extends Service{
    private Notification notification;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder builder;
    private DatabaseReference friendDatabaseNode;
    private DatabaseReference newDataNode;
    private int uploadedImageCount=0;
    private Intent intent;


    @Override
    public void onCreate() {
        Log.i("mytag","service called");
        builder=new NotificationCompat.Builder(this);
        builder.
                setAutoCancel(true).
                setContentTitle("Sending Message").
                setContentText("please wait").
                setSmallIcon(R.mipmap.send_icon);


        notification=builder.build();
        notificationManager= (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.intent=intent;
        if(intent!=null) {
            if (intent.getStringExtra("intentName").equals("sendMessageService")) {
                final MinorDetails receiveObject = (MinorDetails) intent.getSerializableExtra("intents");
                friendDatabaseNode = FirebaseDatabase.getInstance().getReference().child("users/9841339287/friends/9813847444/messages");
                newDataNode = FirebaseDatabase.getInstance().getReference().child("users/9841339287/newData/9813847444/");


                notificationManager.notify(100, notification);
                startForeground(100, notification);//run on the background
                Log.i("tags", "forebackground running ");

                for(int i=0;i<receiveObject.getUploadImages().size();i++){
                    Log.i("mytag", "loops  "+i);

                    //upload all
                    Uri uri =Uri.fromFile(new File(receiveObject.getUploadImages().get(i).getpUrl()));
                    FirebaseStorage.getInstance().getReference()
                            .child("usersdata/"+new File(receiveObject.getUploadImages().get(i).getpUrl()).getName())
                            .putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Log.i("mytag", "uploadimagecount  "+uploadedImageCount);

                            receiveObject.getUploadImages().get(uploadedImageCount).setpUrl(taskSnapshot.getDownloadUrl()+"");
                            Log.i("mytag",taskSnapshot.getMetadata().getContentType()+"");
                            ++uploadedImageCount;
                            if(receiveObject.getUploadImages().size()==uploadedImageCount){
                                Log.i("mytag","send message will be called now");
                                sendMessage(receiveObject);
                            }


                        }
                    });


                }



            }


        }


        return START_NOT_STICKY;//dont call the service again if errors

    }



    private void sendMessage(MinorDetails receiveObject){
        Log.i("mytag","sending url to database");


        friendDatabaseNode.child("message" + receiveObject.getIndex()).setValue(receiveObject, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Log.i("mytag", "error " + databaseError);
                } else {
                    Log.i("mytag", "database " + databaseReference.getDatabase());
                    Log.i("mytag", "key " + databaseReference.getKey());
                    Log.i("mytag", "root " + databaseReference.getRoot());
                    Log.i("mytag", "parent " + databaseReference.getParent());
                    Log.i("mytag", "class " + databaseReference.getClass());
                    builder.setContentText("Successfully sent");
                    notificationManager.notify(100, builder.build());
                    //show a sound
                    MediaPlayer.create(getApplicationContext(), R.raw.completion).start();
                    stopForeground(false);//to remove noti
                    // fication by swap#if true then auto swap by program
                    stopService(intent);//also end the service or may cause error on next send
                }
            }
        });
        newDataNode.child("4").setValue("unseen");

        /*Also save to the offline cache as receiveObject */

    }

}

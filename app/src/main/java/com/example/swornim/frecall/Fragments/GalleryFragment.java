package com.example.swornim.frecall.Fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.swornim.frecall.MainActivity;
import com.example.swornim.frecall.R;
import com.example.swornim.frecall.ReceiveMessageService;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import android.support.v4.content.ContentResolverCompat;

/**
 * Created by swornim on 12/1/17.
 */

public class GalleryFragment extends Fragment implements View.OnClickListener {

    private List<String> pathList=new ArrayList<>();
    private ImageView photos;
    private ImageView videos;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView=inflater.inflate(R.layout.messagefragment_custom_edit,container,false);
        photos= (ImageView) mView.findViewById(R.id.messagefragement_custom_edit_photo);
//        videos= (ImageView) mView.findViewById(R.id.messagefragement_custom_edit_video);
        photos.setOnClickListener(this);
        Log.i("mytag","its working");



        return mView;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.messagefragement_custom_edit_photo:
                //allow multiple photo intents
                Log.i("tags","its working");
                Intent mIntent = new Intent(getContext(), PickImageActivity.class);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 8);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
                startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);
                break;

            case R.id.messagefragement_custom_edit_send:
                break;



        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (resultCode == -1 && requestCode == PickImageActivity.PICKER_REQUEST_CODE) {
            this.pathList = intent.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (this.pathList != null && !this.pathList.isEmpty()) {
                StringBuilder sb=new StringBuilder("");
                for(int i=0;i<pathList.size();i++) {
                    Log.i("tags",pathList.get(i));
                }
            }
        }
    }


}

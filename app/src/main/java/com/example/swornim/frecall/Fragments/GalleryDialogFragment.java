package com.example.swornim.frecall.Fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContentResolverCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.example.swornim.frecall.R;
import com.nileshp.multiphotopicker.photopicker.activity.PickImageActivity;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by swornim on 1/21/18.
 */

public class GalleryDialogFragment extends DialogFragment implements View.OnClickListener{


    private List<String> pathList=new ArrayList<>();
    private ImageView photos;
    private ImageView videos;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View mView=inflater.inflate(R.layout.messagefragment_custom_edit,container,false);
        //add the listeners
        ImageView photos=mView.findViewById(R.id.messagefragement_custom_edit_photo);
        photos.setOnClickListener(this);

        return mView;
    }


    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        getDialog().setTitle("Send Message");
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.messagefragement_custom_edit_photo:
                //allow multiple photo intents
                Log.i("tags","its working");
                Intent mIntent = new Intent(getContext(), PickImageActivity.class);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MAX_IMAGE, 8);
                mIntent.putExtra(PickImageActivity.KEY_LIMIT_MIN_IMAGE, 1);
                startActivityForResult(mIntent, PickImageActivity.PICKER_REQUEST_CODE);
                break;
//
//            case R.id.messagefragement_custom_edit_video:
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Cursor cursor=null;
//
//                        String[] projections=
//                                {
//                                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
//                                        ContactsContract.CommonDataKinds.Phone.NUMBER,
//                                        ContactsContract.CommonDataKinds.Phone._ID,
//
//
//                                };
//
//                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//                            cursor = getContext().
//                                    getContentResolver().query(
//                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null);
//                        }else{
//                            cursor= ContentResolverCompat.query(getContext().getContentResolver(),ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null,null);
//                        }
//
//                        if(cursor!=null){
//                            while(cursor.moveToNext()){
//
//                                Log.i("tags",cursor.getString(cursor.getColumnIndex("DISPLAY_NAME"))+"");
//                                Log.i("tags",cursor.getString(cursor.getColumnIndex("_ID"))+"\n\n");
//
//                                Log.i("tags",cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
//
//                            }
//                        }
//
//
//
//
//
//                    }
//                }).start();

            case R.id.messagefragement_custom_edit_send:
                break;



        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (resultCode == -1 && requestCode == PickImageActivity.PICKER_REQUEST_CODE) {
            this.pathList = intent.getExtras().getStringArrayList(PickImageActivity.KEY_DATA_RESULT);
            if (this.pathList != null && !this.pathList.isEmpty()) {
                StringBuilder sb=new StringBuilder("");
                for(int i=0;i<pathList.size();i++) {
                    Log.i("tags",pathList.get(i));
                }
            }
        }
    }

}

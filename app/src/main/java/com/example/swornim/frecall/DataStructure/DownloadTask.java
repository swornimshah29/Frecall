package com.example.swornim.frecall.DataStructure;

import com.example.swornim.frecall.MinorDetails;

import java.util.List;

/**
 * Created by swornim on 1/24/18.
 */

public class DownloadTask {
    private MinorDetails messageObject;
    private List<String> photoLocation;


    public MinorDetails getMessageObject() {
        return messageObject;
    }

    public void setMessageObject(MinorDetails messageObject) {
        this.messageObject = messageObject;
    }

    public List<String> getPhotoLocation() {
        return photoLocation;
    }

    public void setPhotoLocation(List<String> photoLocation) {
        this.photoLocation = photoLocation;
    }
}

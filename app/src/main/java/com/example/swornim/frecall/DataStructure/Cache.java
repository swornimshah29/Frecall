package com.example.swornim.frecall.DataStructure;

import android.app.MediaRouteActionProvider;

import com.example.swornim.frecall.MinorDetails;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by swornim on 1/22/18.
 */

public class Cache implements Serializable{

    private List<Map<String,List<Map<String,MinorDetails>>>> friends;//N1:{}

    private List<MinorDetails> postdata;
    private List<String> indexes;
    private String status;//seen or delivered or unseen
    private String message;
    private String index;
    private List<MinorDetails> uploadImages;
    private String imageT;
    private String pUrl;//store phones location instead of downloaded url
    private String vUrl;
    private String seen;

    public List<Map<String, List<Map<String, MinorDetails>>>> getFriends() {
        return friends;
    }

    public void setFriends(List<Map<String, List<Map<String, MinorDetails>>>> friends) {
        this.friends = friends;
    }

    public String getSeen() {
        return seen;
    }

    public void setSeen(String seen) {
        this.seen = seen;
    }

    public List<MinorDetails> getPostdata() {
        return postdata;
    }

    public void setPostdata(List<MinorDetails> postdata) {
        this.postdata = postdata;
    }

    public List<String> getIndexes() {
        return indexes;
    }

    public void setIndexes(List<String> indexes) {
        this.indexes = indexes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public List<MinorDetails> getUploadImages() {
        return uploadImages;
    }

    public void setUploadImages(List<MinorDetails> uploadImages) {
        this.uploadImages = uploadImages;
    }

    public String getImageT() {
        return imageT;
    }

    public void setImageT(String imageT) {
        this.imageT = imageT;
    }

    public String getpUrl() {
        return pUrl;
    }

    public void setpUrl(String pUrl) {
        this.pUrl = pUrl;
    }

    public String getvUrl() {
        return vUrl;
    }

    public void setvUrl(String vUrl) {
        this.vUrl = vUrl;
    }
}

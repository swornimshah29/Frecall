package com.example.swornim.frecall;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;


import com.example.swornim.frecall.Fragments.Friendfragment;
import com.example.swornim.frecall.Fragments.GalleryFragment;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class Tabactivity extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPageAdapter viewPageAdapter;
    private ListView dialogListview;



    private ImageView addPhoto;
    private ImageView user_notification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainwrappertoolbar);

        addPhoto=(ImageView) findViewById(R.id.addPhoto);
        user_notification=(ImageView) findViewById(R.id.user_notification);
        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent photoPickIntent = new Intent(Intent.ACTION_PICK);
//                photoPickIntent.setType("image/*");
//                startActivityForResult(photoPickIntent, 0);

                Intent intent1=new Intent(getApplicationContext(),ReceiveMessageService.class);
                intent1.putExtra("intentName","receiveMessageService");
                startService(intent1);

            }
        });

        user_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                fragmentManager = getSupportFragmentManager();
//                fragmentTransaction = fragmentManager.beginTransaction();
//                if(isFragmentInBackstack(fragmentManager,notificationFragment.getClass().getName()) && fragmentTransaction!=null){
//                    fragmentManager.popBackStackImmediate(notificationFragment.getClass().getName(),FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                }
//
//                    fragmentTransaction.
//                            replace(R.id.viewPager, notificationFragment).addToBackStack(NotificationFragment.class.getName()).commit();

                 Dialog dialog=new Dialog(Tabactivity.this,android.R.style.Theme_Wallpaper_NoTitleBar_Fullscreen);
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {

                    }
                });



            }
        });

        tabLayout=(TabLayout) findViewById(R.id.tabLayout);
        viewPager=(ViewPager) findViewById(R.id.viewPager);
        viewPageAdapter=new ViewPageAdapter(getSupportFragmentManager());
        viewPageAdapter.addFragments(new Friendfragment());
        viewPageAdapter.addFragments(new GalleryFragment());

        viewPager.setAdapter(viewPageAdapter);
        tabLayout.setupWithViewPager(viewPager);




    }




    public static boolean isFragmentInBackstack(final FragmentManager fragmentManager, final String fragmentTagName) {
        for (int entry = 0; entry < fragmentManager.getBackStackEntryCount(); entry++) {
            if (fragmentTagName.equals(fragmentManager.getBackStackEntryAt(entry).getName())) {
                return true;
            }
        }
        return false;
    }


    public class ViewPageAdapter extends FragmentPagerAdapter {
        ArrayList<Fragment> fragments=new ArrayList<>();

        public void addFragments( Fragment fragments){
            this.fragments.add(fragments);
        }


        public ViewPageAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {

            return fragments.get(position);
        }

        @Override
        public int getCount() {
           return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0:
                    return "Gallery";
                case 1:
                    return "Friends";

            }
            return null;

        }


    }


}

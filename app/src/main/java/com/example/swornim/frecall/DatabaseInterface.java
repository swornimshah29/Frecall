package com.example.swornim.frecall;

/**
 * Created by swornim on 11/30/17.
 */

public interface DatabaseInterface {
    public void insertMessage(MinorDetails messageObject);
    public MinorDetails getMessage(String phoneNumber,int index);
    public boolean userExists(String phoneNumber);
}
